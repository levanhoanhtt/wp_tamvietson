<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if (is_search()) { ?>
		<meta name="robots" content="noindex, nofollow" />
	<?php } ?>
	<title><?php wp_title(''); ?></title>
	<?php $theme_directory = get_bloginfo('template_directory');?>
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/select2/css/select2.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/jquery.bxslider/jquery.bxslider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/owl.carousel/owl.carousel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/fancyBox/jquery.fancybox.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/lib/jquery-ui/jquery-ui.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/css/animate.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/css/reset.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $theme_directory; ?>/assets/css/responsive.css" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php if (is_singular()) wp_enqueue_script('comment-reply'); ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- TOP BANNER -->
<!--<div id="top-banner" class="top-banner">
    <div class="bg-overlay"></div>
    <div class="container">
        <h1>Special Offer!</h1>
        <h2>Additional 40% OFF For Men & Women Clothings</h2>
        <span>This offer is for online only 7PM to middnight ends in 30th July 2015</span>
        <span class="btn-close"></span>
    </div>
</div>-->
<!-- HEADER -->
<div id="header" class="header">
	<div class="top-header">
		<div class="container">
			<div class="nav-top-links">
				<a class="first-item" href="tel:<?php echo getMyOption('hotline'); ?>"><img alt="phone" src="<?php echo $theme_directory; ?>/assets/images/phone.png" /><?php echo getMyOption('hotline'); ?></a>
				<a href="mailto:<?php echo getMyOption('email'); ?>"><img alt="email" src="<?php echo $theme_directory; ?>/assets/images/email.png" />Contact us today!</a>
			</div>
			<!--<div class="currency">
				<div class="dropdown">
					<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">USD</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Dollar</a></li>
						<li><a href="#">Euro</a></li>
					</ul>
				</div>
			</div>
			<div class="language ">
				<div class="dropdown">
					<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
						<img alt="email" src="<?php //echo $theme_directory; ?>/assets/images/fr.jpg" />French
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#"><img alt="email" src="<?php //echo $theme_directory; ?>/assets/images/en.jpg" />English</a></li>
						<li><a href="#"><img alt="email" src="<?php //echo $theme_directory; ?>/assets/images/fr.jpg" />French</a></li>
					</ul>
				</div>
			</div>-->
			<div class="support-link">
				<a href="#">Services</a>
				<a href="#">Support</a>
			</div>

			<!--<div id="user-info-top" class="user-info pull-right">
				<div class="dropdown">
					<a class="current-open" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"><span>My Account</span></a>
					<ul class="dropdown-menu mega_dropdown" role="menu">
						<li><a href="login.html">Login</a></li>
						<li><a href="#">Compare</a></li>
						<li><a href="#">Wishlists</a></li>
					</ul>
				</div>
			</div>-->
		</div>
	</div>
	<!--/.top-header -->
	<!-- MAIN HEADER -->
	<div class="container main-header">
		<div class="row">
			<div class="col-xs-12 col-sm-3 logo">
				<a href="index.html"><img alt="Tâm Việt Sơn" src="<?php echo getMyOption('header_logo'); ?>" /></a>
			</div>
			<div class="col-xs-7 col-sm-7 header-search-box">
				<form class="form-inline" action="<?php bloginfo('siteurl'); ?>" method="get">
					<div class="form-group form-category">
						<select class="select-category" name="c">
							<option value="0">Chuyên mục</option>
							<?php $cates = get_categories(array('hide_empty' => 0, 'include' => array(3, 4, 5), 'fields' => 'id=>name'));
							$cateId = isset($_GET['c']) ? intval($_GET['c']) : 0;
							foreach($cates as $id => $name) : ?>
								<option value="<?php echo $id; ?>"<?php if($id == $cateId) echo ' selected="selected"'; ?>><?php echo $name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group input-serach">
						<input type="text" name="s" placeholder="Từ khóa" value="<?php echo get_search_query(); ?>">
					</div>
					<button type="submit" class="pull-right btn-search"></button>
				</form>
			</div>
			<!--<div id="cart-block" class="col-xs-5 col-sm-2 shopping-cart-box">
				<a class="cart-link" href="order.html">
					<span class="title">Shopping cart</span>
					<span class="total">2 items - 122.38 €</span>
					<span class="notify notify-left">2</span>
				</a>
				<div class="cart-block">
					<div class="cart-block-content">
						<h5 class="cart-title">2 Items in my cart</h5>
						<div class="cart-block-list">
							<ul>
								<li class="product-info">
									<div class="p-left">
										<a href="#" class="remove_link"></a>
										<a href="#">
											<img class="img-responsive" src="<?php //echo $theme_directory; ?>/assets/data/product-100x122.jpg" alt="p10">
										</a>
									</div>
									<div class="p-right">
										<p class="p-name">Donec Ac Tempus</p>
										<p class="p-rice">61,19 €</p>
										<p>Qty: 1</p>
									</div>
								</li>
								<li class="product-info">
									<div class="p-left">
										<a href="#" class="remove_link"></a>
										<a href="#">
											<img class="img-responsive" src="<?php //echo $theme_directory; ?>/assets/data/product-s5-100x122.jpg" alt="p10">
										</a>
									</div>
									<div class="p-right">
										<p class="p-name">Donec Ac Tempus</p>
										<p class="p-rice">61,19 €</p>
										<p>Qty: 1</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="toal-cart">
							<span>Total</span>
							<span class="toal-price pull-right">122.38 €</span>
						</div>
						<div class="cart-buttons">
							<a href="order.html" class="btn-check-out">Checkout</a>
						</div>
					</div>
				</div>
			</div>-->
		</div>
	</div>
	<!-- END MANIN HEADER -->
	<div id="nav-top-menu" class="nav-top-menu">
		<div class="container">
			<div class="row">
				<div class="col-sm-3" id="box-vertical-megamenus">
					<div class="box-vertical-megamenus">
						<h4 class="title">
							<span class="title-menu">Sản phẩm</span>
							<span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
						</h4>
						<div class="vertical-menu-content is-home">
							<?php wp_nav_menu(array('theme_location' => 'product_nav', 'container'=>'','menu_class'=> 'vertical-menu-list')); ?>
							<!--<div class="all-category"><span class="open-cate">All Categories</span></div>-->
						</div>
					</div>
				</div>
				<div id="main-menu" class="col-sm-9 main-menu">
					<nav class="navbar navbar-default">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
									<i class="fa fa-bars"></i>
								</button>
								<a class="navbar-brand" href="#">MENU</a>
							</div>
							<div id="navbar" class="navbar-collapse collapse">
								<?php $menu = wp_nav_menu(array('echo' => false, 'theme_location' => 'main_nav', 'container'=>'','menu_class'=> 'nav navbar-nav'));
								echo str_replace('current-menu-item', 'current-menu-item active', $menu); ?>
							</div>
						</div>
					</nav>
				</div>
			</div>
			<!-- userinfo on top-->
			<div id="form-search-opntop"></div>
		</div>
	</div>
</div>
<!-- end header -->