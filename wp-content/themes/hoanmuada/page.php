<?php get_header(); ?>
	<div class="columns-container">
		<div class="container" id="columns">
			<?php get_template_part('templates/breadcrumb'); ?>
			<!-- row -->
			<div class="row">
				<?php get_sidebar('news'); ?>
				<div class="center_column col-xs-12 col-sm-9" id="center_column">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h2 class="page-heading">
						<span class="page-heading-title2"><?php the_title(); ?></span>
					</h2>
					<div class="content-text clearfix">
						<?php the_content(); ?>
					</div>
					<?php endwhile; endif; ?>
				</div>
			</div>
		</div>
	</div>
	<input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>