<?php get_header(); ?>
    <?php $theme_directory = get_bloginfo('template_directory');?>
    <div class="columns-container">
        <div class="container" id="columns">
            <?php get_template_part('templates/breadcrumb'); ?>
            <div class="row">
                <?php get_sidebar('news'); ?>
                <div class="center_column col-xs-12 col-sm-9" id="center_column">
                    <?php if (have_posts()) : ?>
                    <h2 class="page-heading">
                        <span class="page-heading-title2"><?php single_cat_title(); ?></span>
                    </h2>
                    <div class="sortPagiBar clearfix">
                        <!--<span class="page-noite">Showing 1 to 7 of 45 (15 Pages)</span>-->
                        <div class="bottom-pagination" id="divPaging01">
                            <?php page_nav(); ?>
                        </div>
                    </div>
                    <ul class="blog-posts">
                        <?php $newsCateUrl = get_category_link(7); ?>
                        <?php while (have_posts()) : the_post(); ?>
                            <li class="post-item">
                                <article class="entry">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="entry-thumb image-hover2">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail(array(345, 244)); ?>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="entry-ci">
                                                <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                                <div class="entry-meta-data">
                                                    <span class="author">
                                                    <i class="fa fa-user"></i>
                                                    <a href="#">Admin</a></span>
                                                    <span class="cat">
                                                        <i class="fa fa-folder-o"></i>
                                                        <a href="<?php echo $newsCateUrl; ?>">Tin tức</a>
                                                    </span>
                                                    <span class="comment-count">
                                                        <i class="fa fa-comment-o"></i> 3
                                                    </span>
                                                    <span class="date"><i class="fa fa-calendar"></i> <?php the_time('d/m/y H:i:s'); ?></span>
                                                </div>
                                                <!--<div class="post-star">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                    <span>(7 votes)</span>
                                                </div>-->
                                                <div class="entry-excerpt">
                                                    <?php echo strip_tags(get_the_excerpt()); ?>
                                                </div>
                                                <div class="entry-more">
                                                    <a href="<?php the_permalink(); ?>">Đọc thêm</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                    <div class="sortPagiBar clearfix">
                        <div class="bottom-pagination" id="divPaging02"></div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>