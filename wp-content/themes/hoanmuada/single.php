
<?php get_header(); ?>
    <div class="columns-container">
        <div class="container" id="columns">
            <?php get_template_part('templates/breadcrumb'); ?>
            <div class="row">
                <?php get_sidebar(); ?>
                <div class="center_column col-xs-12 col-sm-9" id="center_column">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                    <?php $productId = get_the_ID(); ?>
                    <div id="product">
                        <div class="primary-box row">
                            <div class="pb-left-column col-xs-12 col-sm-6">
                                <?php $imageTag = get_the_post_thumbnail($productId, 'full');
                                $imageSrc = '';
                                if($imageTag) $imageSrc = getImageSrc($imageTag);
                                if(!empty($imageSrc)) : ?>
                                <div class="product-image">
                                    <div class="product-full">
                                        <img id="product-zoom" src='<?php echo $imageSrc; ?>' data-zoom-image="<?php echo $imageSrc; ?>"/>
                                    </div>
                                    <!--<div class="product-img-thumb" id="gallery_01">
                                        <ul class="owl-carousel" data-items="3" data-nav="true" data-dots="false" data-margin="20" data-loop="false">
                                            <li>
                                                <a href="#" data-image="assets/data/product-s3-420x512.jpg" data-zoom-image="assets/data/product-s3-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-s3-100x122.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/product-s2-420x512.jpg" data-zoom-image="assets/data/product-s2-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-s2-100x122.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/product-420x512.jpg" data-zoom-image="assets/data/product-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-100x122.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/product-s4-420x512.jpg" data-zoom-image="assets/data/product-s4-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-s4-100x122.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/product-s5-420x512.jpg" data-zoom-image="assets/data/product-s5-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-s5-100x122.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" data-image="assets/data/product-s6-420x512.jpg" data-zoom-image="assets/data/product-s6-850x1036.jpg">
                                                    <img id="product-zoom"  src="assets/data/product-s6-100x122.jpg" />
                                                </a>
                                            </li>
                                        </ul>
                                    </div>-->
                                </div>
                                <?php endif; ?>
                            </div>
                            <div class="pb-right-column col-xs-12 col-sm-6">
                                <h1 class="product-name"><?php the_title(); ?></h1>
                                <div class="product-comments">
                                    <div class="product-star">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-half-o"></i>
                                    </div>
                                    <!--<div class="comments-advices">
                                        <a href="#">Based  on 3 ratings</a>
                                        <a href="#"><i class="fa fa-pencil"></i> write a review</a>
                                    </div>-->
                                </div>
                                <!--div class="product-price-group">
                                    <span class="price">$38.95</span>
                                    <span class="old-price">$52.00</span>
                                    <span class="discount">-30%</span>
                                </div-->
                                <div class="info-orther">
                                    <p>Mã: #TVS<?php echo $productId; ?></p>
                                    <p>Tình trạng: <span class="in-stock">Còn hàng</span></p>
                                    <!--p>Condition: New</p-->
                                </div>
                                <div class="product-desc"><?php echo strip_tags(get_the_excerpt()); ?></div>
                                <!--div class="form-option">
                                    <p class="form-option-title">Available Options:</p>
                                    <div class="attributes">
                                        <div class="attribute-label">Color:</div>
                                        <div class="attribute-list">
                                            <ul class="list-color">
                                                <li style="background:#0c3b90;"><a href="#">red</a></li>
                                                <li style="background:#036c5d;" class="active"><a href="#">red</a></li>
                                                <li style="background:#5f2363;"><a href="#">red</a></li>
                                                <li style="background:#ffc000;"><a href="#">red</a></li>
                                                <li style="background:#36a93c;"><a href="#">red</a></li>
                                                <li style="background:#ff0000;"><a href="#">red</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="attributes">
                                        <div class="attribute-label">Qty:</div>
                                        <div class="attribute-list product-qty">
                                            <div class="qty">
                                                <input id="option-product-qty" type="text" value="1">
                                            </div>
                                            <div class="btn-plus">
                                                <a href="#" class="btn-plus-up">
                                                    <i class="fa fa-caret-up"></i>
                                                </a>
                                                <a href="#" class="btn-plus-down">
                                                    <i class="fa fa-caret-down"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="attributes">
                                        <div class="attribute-label">Size:</div>
                                        <div class="attribute-list">
                                            <select>
                                                <option value="1">X</option>
                                                <option value="2">XL</option>
                                                <option value="3">XXL</option>
                                            </select>
                                            <a id="size_chart" class="fancybox" href="assets/data/size-chart.jpg">Size Chart</a>
                                        </div>

                                    </div>
                                </div-->
                                <!--div class="form-action">
                                    <div class="button-group">
                                        <a class="btn-add-cart" href="#">Add to cart</a>
                                    </div>
                                    <div class="button-group">
                                        <a class="wishlist" href="#"><i class="fa fa-heart-o"></i>
                                            <br>Wishlist</a>
                                        <a class="compare" href="#"><i class="fa fa-signal"></i>
                                            <br>
                                            Compare</a>
                                    </div>
                                </div-->
                                <!--div class="form-share">
                                    <div class="sendtofriend-print">
                                        <a href="javascript:print();"><i class="fa fa-print"></i> Print</a>
                                        <a href="#"><i class="fa fa-envelope-o fa-fw"></i>Send to a friend</a>
                                    </div>
                                    <div class="network-share">
                                    </div>
                                </div-->
                            </div>
                        </div>
                        <div class="product-tab">
                            <ul class="nav-tab">
                                <li class="active">
                                    <a aria-expanded="false" data-toggle="tab" href="#product-detail">MÔ TẢ</a>
                                </li>
                                <!--<li>
                                    <a aria-expanded="true" data-toggle="tab" href="#information">THÔNG TIN</a>
                                </li>-->
                                <li>
                                    <a data-toggle="tab" href="#reviews">BÌNH LUẬN</a>
                                </li>
                                <!--li>
                                    <a data-toggle="tab" href="#extra-tabs">Extra Tabs</a>
                                </li>
                                <li>
                                    <a data-toggle="tab" href="#guarantees">guarantees</a>
                                </li-->
                            </ul>
                            <div class="tab-container">
                                <div id="product-detail" class="tab-panel active"><?php the_content(); ?></div>
                                <!--<div id="information" class="tab-panel">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td width="200">Compositions</td>
                                            <td>Cotton</td>
                                        </tr>
                                        <tr>
                                            <td>Styles</td>
                                            <td>Girly</td>
                                        </tr>
                                        <tr>
                                            <td>Properties</td>
                                            <td>Colorful Dress</td>
                                        </tr>
                                    </table>
                                </div>-->
                                <div id="reviews" class="tab-panel"><?php comments_template(); ?></div>
                                <!--div id="extra-tabs" class="tab-panel">
                                    <p>Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Sed lectus. Sed a libero. Vestibulum eu odio.</p>

                                    <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>

                                    <p>Maecenas nec odio et ante tincidunt tempus. Vestibulum suscipit nulla quis orci. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Aenean ut eros et nisl sagittis vestibulum. Aliquam eu nunc.</p>
                                </div>
                                <div id="guarantees" class="tab-panel">
                                    <p>Phasellus accumsan cursus velit. Pellentesque egestas, neque sit amet convallis pulvinar, justo nulla eleifend augue, ac auctor orci leo non est. Sed lectus. Sed a libero. Vestibulum eu odio.</p>

                                    <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>

                                    <p>Maecenas nec odio et ante tincidunt tempus. Vestibulum suscipit nulla quis orci. Aenean tellus metus, bibendum sed, posuere ac, mattis non, nunc. Aenean ut eros et nisl sagittis vestibulum. Aliquam eu nunc.</p>
                                    <p>Maecenas vestibulum mollis diam. In consectetuer turpis ut velit. Curabitur at lacus ac velit ornare lobortis. Praesent ac sem eget est egestas volutpat. Nam eget dui.</p>
                                </div-->
                            </div>
                        </div>
                        <?php $tagIds = wp_get_post_tags($productId, array('fields' => 'ids'));
                        $args=array(
                            'tag__in' => $tagIds,
                            'post__not_in' => array($productId),
                            'posts_per_page'=> 4,
                            'category__not_in' => array(7),
                            'orderby' => 'rand'
                        );
                        $news = new WP_Query($args);
                        if($news->have_posts()) : ?>
                        <div class="page-product-box">
                            <h3 class="heading">SẢN PHẨM LIÊN QUAN</h3>
                            <ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":3}}'>
                                <?php while($news->have_posts()) : $news->the_post(); ?>
                                    <li>
                                        <div class="product-container">
                                            <div class="left-block">
                                                <a href="<?php the_permalink(); ?>">
                                                    <?php the_post_thumbnail(array(300, 366), array('class' => 'img-responsive')); ?>
                                                </a>
                                                <!--<div class="quick-view">
                                                    <a title="Add to my wishlist" class="heart" href="#"></a>
                                                    <a title="Add to compare" class="compare" href="#"></a>
                                                    <a title="Quick view" class="search" href="#"></a>
                                                </div>
                                                <div class="add-to-cart">
                                                    <a title="Add to Cart" href="#add">Add to Cart</a>
                                                </div>-->
                                            </div>
                                            <div class="right-block">
                                                <h5 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                                <div class="product-star">
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star-half-o"></i>
                                                </div>
                                                <!--<div class="content_price">
                                                    <span class="price product-price">$38,95</span>
                                                    <span class="price old-price">$52,00</span>
                                                </div>-->
                                            </div>
                                        </div>
                                    </li>
                                <?php endwhile; wp_reset_postdata();?>
                            </ul>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="pageType" value="product">
<?php get_footer(); ?>