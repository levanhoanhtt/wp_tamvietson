<?php
if( function_exists('acf_add_options_page') ) {
    $args = array(
        'page_title' => 'Theme Options',
        'menu_title' => '',
        'menu_slug' => '',
        'capability' => 'edit_posts',
        'position' => false,
        'parent_slug' => '',
        'icon_url' => false,
        'redirect' => true,
        'post_id' => 'theme_options',
        'autoload' => false
    );
    acf_add_options_page($args);

    $args = array(
        'page_title' => 'Home Options',
        'menu_title' => '',
        'menu_slug' => '',
        'capability' => 'edit_posts',
        'position' => false,
        'parent_slug' => '',
        'icon_url' => false,
        'redirect' => true,
        'post_id' => 'home_options',
        'autoload' => false
    );
    acf_add_options_page($args);
}