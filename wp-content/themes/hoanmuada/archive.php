<?php get_header(); ?>
    <?php $theme_directory = get_bloginfo('template_directory');?>
    <div class="columns-container">
        <div class="container" id="columns">
            <?php get_template_part('templates/breadcrumb'); ?>
            <div class="row">
                <?php get_sidebar(); ?>
                <div class="center_column col-xs-12 col-sm-9" id="center_column">
                    <div class="category-slider">
                        <ul class="owl-carousel owl-style2" data-dots="false" data-loop="true" data-nav = "true" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-items="1">
                            <li>
                                <img src="<?php echo $theme_directory; ?>/assets/data/category-slide.jpg" alt="category-slider">
                            </li>
                            <li>
                                <img src="<?php echo $theme_directory; ?>/assets/data/slide-cart2.jpg" alt="category-slider">
                            </li>
                        </ul>
                    </div>
                    <?php $cateName = single_cat_title('', false); ?>
                    <div class="subcategories">
                        <ul>
                            <li class="current-categorie">
                                <a href="#"><?php echo $cateName; ?></a>
                            </li>
                            <!--<li>
                                <a href="#">Tops</a>
                            </li>
                            <li>
                                <a href="#">Dresses</a>
                            </li>
                            <li>
                                <a href="#">Bags & Shoes</a>
                            </li>
                            <li>
                                <a href="#">Pants</a>
                            </li>
                            <li>
                                <a href="#">Blouses</a>
                            </li>-->
                        </ul>
                    </div>
                    <div id="view-product-list" class="view-product-list">
                        <h2 class="page-heading">
                            <span class="page-heading-title"><?php echo $cateName; ?></span>
                        </h2>
                        <ul class="display-product-option">
                            <li class="view-as-grid selected">
                                <span>grid</span>
                            </li>
                            <li class="view-as-list">
                                <span>list</span>
                            </li>
                        </ul>
                        <ul class="row product-list grid">
                            <?php while (have_posts()) : the_post(); ?>
                                <li class="col-sx-12 col-sm-4">
                                    <div class="product-container">
                                        <div class="left-block">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail(array(300, 366), array('class' => 'img-responsive')); ?>
                                            </a>
                                            <!--<div class="quick-view">
                                                <a title="Add to my wishlist" class="heart" href="#"></a>
                                                <a title="Add to compare" class="compare" href="#"></a>
                                                <a title="Quick view" class="search" href="#"></a>
                                            </div>
                                            <div class="add-to-cart">
                                                <a title="Add to Cart" href="#add">Add to Cart</a>
                                            </div>-->
                                        </div>
                                        <div class="right-block">
                                            <h5 class="product-name"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                            <div class="product-star">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star-half-o"></i>
                                            </div>
                                            <!--<div class="content_price">
                                                <span class="price product-price">$38,95</span>
                                                <span class="price old-price">$52,00</span>
                                            </div>-->
                                            <div class="info-orther">
                                                <p>Mã: #TVS<?php the_ID(); ?></p>
                                                <p class="availability">Tình trạng: <span>Còn hàng</span></p>
                                                <div class="product-desc">
                                                    <?php echo strip_tags(get_the_excerpt()); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </div>
                    <div class="sortPagiBar">
                        <div class="bottom-pagination">
                            <?php page_nav(); ?>
                        </div>
                        <!--<div class="show-product-item">
                            <select name="">
                                <option value="">Show 18</option>
                                <option value="">Show 20</option>
                                <option value="">Show 50</option>
                                <option value="">Show 100</option>
                            </select>
                        </div>
                        <div class="sort-product">
                            <select>
                                <option value="">Product Name</option>
                                <option value="">Price</option>
                            </select>
                            <div class="sort-product-icon">
                                <i class="fa fa-sort-alpha-asc"></i>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>