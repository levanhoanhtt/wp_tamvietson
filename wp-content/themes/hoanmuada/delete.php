<div class="block left-module">
    <p class="title_block">SẢN PHẨM MỚI NHẤT</p>
    <div class="block_content">
        <div class="owl-carousel owl-best-sell" data-loop="true" data-nav = "false" data-margin = "0" data-autoplayTimeout="1000" data-autoplay="true" data-autoplayHoverPause = "true" data-items="1">
            <ul class="products-block best-sell">
                <?php global $post;
                $news = get_posts(array('posts_per_page'=> 3,'category'=> 4));
                foreach($news as $post) : setup_postdata($post); ?>
                    <li>
                        <div class="products-block-left">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail(array(75,92)); ?>
                            </a>
                        </div>
                        <div class="products-block-right">
                            <p class="product-name">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </p>
                            <!--p class="product-price">$38,95</p-->
                            <p class="product-star">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-half-o"></i>
                            </p>
                        </div>
                    </li>
                <?php endforeach; wp_reset_postdata(); ?>

            </ul>
            <ul class="products-block best-sell">
                <li>
                    <div class="products-block-left">
                        <a href="#">
                            <img src="assets/data/p13.jpg" alt="SPECIAL PRODUCTS">
                        </a>
                    </div>
                    <div class="products-block-right">
                        <p class="product-name">
                            <a href="#">Woman Within Plus Size Flared</a>
                        </p>
                        <p class="product-price">$38,95</p>
                        <p class="product-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </p>
                    </div>
                </li>
                <li>
                    <div class="products-block-left">
                        <a href="#">
                            <img src="assets/data/p14.jpg" alt="SPECIAL PRODUCTS">
                        </a>
                    </div>
                    <div class="products-block-right">
                        <p class="product-name">
                            <a href="#">Woman Within Plus Size Flared</a>
                        </p>
                        <p class="product-price">$38,95</p>
                        <p class="product-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </p>
                    </div>
                </li>
                <li>
                    <div class="products-block-left">
                        <a href="#">
                            <img src="assets/data/p15.jpg" alt="SPECIAL PRODUCTS">
                        </a>
                    </div>
                    <div class="products-block-right">
                        <p class="product-name">
                            <a href="#">Plus Size Rock Star Skirt</a>
                        </p>
                        <p class="product-price">$38,95</p>
                        <p class="product-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                        </p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>