<?php get_header(); ?>
<?php $theme_directory = get_bloginfo('template_directory');?>
	<div class="columns-container">
		<div class="container" id="columns">
			<?php get_template_part('templates/breadcrumb'); ?>
			<div class="row">
				<div class="center_column col-xs-12 col-sm-12" id="center_column">
					<h2 class="page-heading">
						<span class="page-heading-title2">Tìm kiếm "<?php echo get_search_query(); ?>"</span>
					</h2>
					<?php if(isset($_GET['c']) && is_numeric($_GET['c']) && $_GET['c'] > 0) {
						global $wp_query;
						query_posts(
							array_merge(
								$wp_query->query,
								array('cat' => $_GET['c'])
							)
						);
					} ?>
					<div class="sortPagiBar clearfix">
						<span class="page-noite">Tìm thấy <?php global $wp_query; echo $wp_query->found_posts ?> kết quả</span>
						<div class="bottom-pagination" id="divPaging01">
							<?php page_nav(); ?>
						</div>
					</div>
					<ul class="blog-posts">
						<?php while (have_posts()) : the_post(); ?>
							<li class="post-item">
								<article class="entry">
									<div class="row">
										<div class="col-sm-12">
											<div class="entry-ci">
												<h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
												<div class="entry-excerpt">
													<?php echo strip_tags(get_the_excerpt()); ?>
												</div>
												<div class="entry-more">
													<a href="<?php the_permalink(); ?>">Đọc thêm</a>
												</div>
											</div>
										</div>
									</div>
								</article>
							</li>
						<?php endwhile; ?>
					</ul>
					<div class="sortPagiBar clearfix">
						<div class="bottom-pagination" id="divPaging02"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>