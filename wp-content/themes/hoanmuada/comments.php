<?php
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');
?>

<?php if ( have_comments() ) : ?>
<div class="single-box">
	<h2>Bình luận</h2>
	<div class="comment-list">
		<ul>
			<?php $theme_directory = get_bloginfo('template_directory');
			$comments = get_comments(array('post_id' => $post->ID, 'status' => 'approve'));
			foreach($comments as $c) : ?>
				<li id="comment-<?php echo $c->comment_ID; ?>">
					<div class="avartar">
						<img src="<?php echo $theme_directory; ?>/assets/data/avatar.png" alt="Avatar">
					</div>
					<div class="comment-body">
						<div class="comment-meta">
							<span class="author"><a href="mailto:<?php echo $c->comment_author_email; ?>"><?php echo $c->comment_author; ?></a></span>
							<span class="date"><?php echo formatDateComment($c->comment_date); ?></span>
						</div>
						<div class="comment"><?php echo $c->comment_content; ?></div>
					</div>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

<?php if ( comments_open() ) : ?>
	<div class="single-box">
		<h2><?php comment_form_title( 'Bình luận', 'Bình luận cho %s' ); ?></h2>
		<div class="coment-form">
			<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
				<div class="row">
					<div class="col-sm-6">
						<label for="name">Họ tên</label>
						<input name="author" type="text" class="form-control" required>
					</div>
					<div class="col-sm-6">
						<label for="email">Email</label>
						<input name="email" type="text" class="form-control" required>
					</div>
					<div class="col-sm-12">
						<label for="website">Website URL</label>
						<input name="url" type="text" class="form-control" required>
					</div>
					<div class="col-sm-12">
						<label for="message">Nội dung</label>
						<textarea name="comment" rows="8" class="form-control" required></textarea>
					</div>
				</div>
				<button class="btn-comment">Gửi</button>
				<?php comment_id_fields(); ?>
				<?php do_action('comment_form', $post->ID); ?>
			</form>
		</div>
	</div>
<?php endif; ?>
