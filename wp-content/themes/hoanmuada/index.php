<?php get_header(); ?>
	<?php $theme_directory = get_bloginfo('template_directory');?>
	<?php $homeOptions = array();
	$fields = get_field_objects('home_options');
	foreach( $fields as $label => $field ) $homeOptions[$label] = $field['value']; ?>
	<?php if(isset($homeOptions['home_sliders'])) : ?>
	<!-- Home slideder-->
	<div id="home-slider">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 slider-left"></div>
				<div class="col-sm-9 header-top-right">
					<div class="homeslider">
						<div class="content-slide">
							<ul id="contenhomeslider">
								<?php foreach($homeOptions['home_sliders'] as $image) : ?>
									<li><img alt="" src="<?php echo $image['image']; ?>"/></li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
					<div class="header-banner banner-opacity">
						<a href="<?php echo isset($homeOptions['home_slider_ads_url']) ? $homeOptions['home_slider_ads_url'] : 'javascript:void(0)'; ?>"><img src="<?php echo isset($homeOptions['home_slider_ads_image']) ? $homeOptions['home_slider_ads_image'] : $theme_directory.'/assets/data/ads1.jpg'; ?>"/></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END Home slideder-->
	<?php endif; ?>
	<!--div class="container">
		<div class="service ">
			<div class="col-xs-6 col-sm-3 service-item">
				<div class="icon">
					<img alt="services" src="<?php echo $theme_directory; ?>/assets/data/s1.png" />
				</div>
				<div class="info">
					<a href="#"><h3>Free Shipping</h3></a>
					<span>On order over $200</span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 service-item">
				<div class="icon">
					<img alt="services" src="<?php echo $theme_directory; ?>/assets/data/s2.png" />
				</div>
				<div class="info">
					<a href="#"><h3>30-day return</h3></a>
					<span>Moneyback guarantee</span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 service-item">
				<div class="icon">
					<img alt="services" src="<?php echo $theme_directory; ?>/assets/data/s3.png" />
				</div>

				<div class="info" >
					<a href="#"><h3>24/7 support</h3></a>
					<span>Online consultations</span>
				</div>
			</div>
			<div class="col-xs-6 col-sm-3 service-item">
				<div class="icon">
					<img alt="services" src="<?php echo $theme_directory; ?>/assets/data/s4.png" />
				</div>
				<div class="info">
					<a href="#"><h3>SAFE SHOPPING</h3></a>
					<span>Safe Shopping Guarantee</span>
				</div>
			</div>
		</div>
	</div>-->
	<div class="content-page">
		<div class="container">
			<?php $navCorlor = array('nav-menu-red', 'nav-menu-green', 'nav-menu-orange', 'nav-menu-blue', 'nav-menu-blue2', 'nav-menu-gray');

			if(isset($homeOptions['products_section'])){
				$i = -1;
				foreach($homeOptions['products_section'] as $section){
					$i++; ?>
					<div class="category-featured">
						<nav class="navbar nav-menu <?php echo $navCorlor[$i]; ?> show-brand">
							<div class="container">
								<div class="navbar-brand">
									<a href="#">
										<!--img alt="fashion" src="<?php echo $theme_directory; ?>/assets/data/fashion.png" /--><?php echo $section['main_category'][0]->name; ?>
									</a>
								</div>
								<!--span class="toggle-menu"></span>

								<div class="collapse navbar-collapse">
									<ul class="nav navbar-nav">
										<li class="active"><a data-toggle="tab" href="#tab-4">Best Seller</a></li>
										<li><a data-toggle="tab" href="#tab-5">Most Viewed</a></li>
										<li><a href="#">Women</a></li>
										<li><a href="#">Men</a></li>
										<li><a href="#">Kids</a></li>
										<li><a href="#">Accessories</a></li>
									</ul>
								</div-->
							</div>
							<!--div id="elevator-1" class="floor-elevator">
								<a href="#" class="btn-elevator up disabled fa fa-angle-up"></a>
								<a href="#elevator-2" class="btn-elevator down fa fa-angle-down"></a>
							</div-->
						</nav>
						<!--div class="category-banner">
							<div class="col-sm-6 banner">
								<a href="#"><img alt="ads2" class="img-responsive" src="<?php echo $theme_directory; ?>/assets/data/ads2.jpg" /></a>
							</div>
							<div class="col-sm-6 banner">
								<a href="#"><img alt="ads2" class="img-responsive" src="<?php echo $theme_directory; ?>/assets/data/ads3.jpg" /></a>
							</div>
						</div-->
						<div class="product-featured clearfix">
							<div class="banner-featured">
								<div class="featured-text"><span>featured</span></div>
								<div class="banner-img">
									<a href="<?php echo get_permalink($section['featured_product']->ID); ?>">
										<?php echo get_the_post_thumbnail($p->ID, array(234,350)); ?>
									</a>
								</div>
							</div>
							<div class="product-featured-content">
								<div class="product-featured-list">
									<div class="tab-container">
										<div class="tab-panel active" id="tab-4">
											<ul class="product-list owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "0" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":3},"1000":{"items":4}}'>
												<?php foreach($section['list_product'] as $p){
													$link = get_permalink($p->ID); ?>
												<li>
													<div class="left-block">
														<a href="<?php echo $link; ?>">
															<?php echo get_the_post_thumbnail($p->ID, array(268,327), array('class'=>'img-responsive')); ?>
														</a>
														<!--div class="quick-view">
															<a title="Add to my wishlist" class="heart" href="#"></a>
															<a title="Add to compare" class="compare" href="#"></a>
															<a title="Quick view" class="search" href="#"></a>
														</div>
														<div class="add-to-cart">
															<a title="Add to Cart" href="#">Add to Cart</a>
														</div-->
													</div>
													<div class="right-block">
														<h5 class="product-name"><a href="<?php echo $link;  ?>"><?php echo $p->post_title; ?></a></h5>
														<!--div class="content_price">
															<span class="price product-price">$38,95</span>
															<span class="price old-price">$52,00</span>
														</div-->
														<div class="product-star">
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star-half-o"></i>
														</div>
													</div>
												</li>
												<?php } ?>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php }
			}
			?>

			<!--div class="row banner-bottom">
				<div class="col-sm-6">
					<div class="banner-boder-zoom">
						<a href="#"><img alt="ads" class="img-responsive" src="<?php echo $theme_directory; ?>/assets/data/ads17.jpg" /></a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="banner-boder-zoom">
						<a href="#"><img alt="ads" class="img-responsive" src="<?php echo $theme_directory; ?>/assets/data/ads18.jpg" /></a>
					</div>
				</div>
			</div-->

		</div>
	</div>

	<input type="text" hidden="hidden" id="pageType" value="home">
<?php get_footer(); ?>