<?php $theme_directory = get_bloginfo('template_directory');?>
<!-- Footer -->
<footer id="footer">
	<div class="container">
		<!-- introduce-box -->
		<div id="introduce-box" class="row">
			<div class="col-md-4">
				<div id="address-box">
					<a href="#"><img src="<?php echo getMyOption('footer_logo'); ?>" alt="" /></a>
					<div id="address-list">
						<div class="tit-name">Địa chỉ:</div>
						<div class="tit-contain"><?php echo getMyOption('address'); ?></div>
						<div class="tit-name">Phone:</div>
						<div class="tit-contain"><?php echo getMyOption('phone'); ?></div>
						<div class="tit-name">Email:</div>
						<div class="tit-contain"><?php echo getMyOption('email'); ?></div>
					</div>
				</div>
			</div>
			<div class="col-md-5">
				<div class="row">
					<div class="col-sm-6">
						<div class="introduce-title">Tâm Việt Sơn</div>
						<?php wp_nav_menu(array('theme_location' => 'footer_company_nav', 'container'=>'','menu_class'=> 'introduce-list', 'menu_id' => 'introduce-company')); ?>
					</div>
					<div class="col-sm-6">
						<div class="introduce-title">Sản phẩm</div>
						<?php wp_nav_menu(array('theme_location' => 'product_nav', 'container'=>'','menu_class'=> 'introduce-list', 'menu_id' => 'introduce-support')); ?>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div id="contact-box">
					<!--<div class="introduce-title">Newsletter</div>
					<div class="input-group" id="mail-box">
						<input type="text" placeholder="Email"/>
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button">OK</button>
                          </span>
					</div>-->
					<div class="introduce-title">Mạng xã hội</div>
					<div class="social-link">
						<a href="<?php echo getMyOption('facebook'); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
						<!--a href="#"><i class="fa fa-pinterest-p"></i></1--a>
						<a href="#"><i class="fa fa-vk"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a-->
						<a href="<?php echo getMyOption('google_plus');  ?>" target="_blank"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>

			</div>
		</div><!-- /#introduce-box -->
		<div id="footer-menu-box">
			<p class="text-center">Copyrights &#169; 2016 TamVietSon. All Rights Reserved. Designed by Hoàn Mưa Đá</p>
		</div><!-- /#footer-menu-box -->
	</div>
</footer>
<a href="#" class="scroll_top" title="Scroll to Top" style="display: inline;">Scroll</a>
<!-- Script-->
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/jquery/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/select2/js/select2.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/jquery.bxslider/jquery.bxslider.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/owl.carousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/jquery.countdown/jquery.countdown.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/jquery.actual.min.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/fancyBox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/lib/jquery.elevatezoom.js"></script>
<script type="text/javascript" src="<?php echo $theme_directory; ?>/assets/js/theme-script.js"></script>
<?php wp_footer(); ?>
</body>
</html>		