<?php
// Add RSS links to <head> section
automatic_feed_links();
// Clean up the <head>
function removeHeadLinks() {
	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wlwmanifest_link');
}
add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// create post thumbnail
if (function_exists('add_theme_support')) {
	add_theme_support('post-thumbnails');
}

// create menu support for theme.
if (function_exists('register_nav_menus')) {
	register_nav_menus(
		array(
			'main_nav' => 'Main Navigation Menu',
			'product_nav' => 'Product Navigation Menu',
			'footer_company_nav' => 'Footer Company Navigation Menu'
		)
	);
}

//add_excerpts_to_pages
add_action('init', 'my_add_excerpts_to_pages');
function my_add_excerpts_to_pages(){
	add_post_type_support('page', 'excerpt');
}

if ( !is_admin() ) {
	global $myOptions;
	$myOptions = array();
	$fields = get_field_objects('theme_options');
	foreach( $fields as $label => $field ) $myOptions[$label] = $field['value'];
}
function getMyOption($optionName, $defaultValue = ""){
	if(!empty($optionName)){
		global $myOptions;
		if($myOptions){
			if(isset($myOptions[$optionName])) return $myOptions[$optionName];
			else{
				$value = get_field($optionName, 'theme_options');
				if(!$value) $value = $defaultValue;
				return $value;
			}
		}
		else{
			$value = get_field($optionName, 'theme_options');
			if(!$value) $value = $defaultValue;
			return $value;
		}
	}
	return $defaultValue;
}

//template for single page
function new_single_template($single_template){
	if(has_category(7)) $single_template = dirname( __FILE__ ) . '/single-news.php';
	return $single_template;
}
add_filter( 'single_template', 'new_single_template');

//admin css
function admin_css(){ ?>
	<style>
		tr#advanced-custom-fields-pro-update{display: none !important;}
		img.taxonomy-image{max-width: 100%;}
		body.toplevel_page_acf-options-home-options #post-body-content{width: 135% !important;}
	</style>
	<?php
}
add_action( 'admin_head', 'admin_css' );

//paging
function page_nav(){
	//if(is_singular()) return;
	global $wp_query;
	if($wp_query->max_num_pages <= 1) return;
	$paged = get_query_var('paged') ? absint(get_query_var( 'paged')) : 1;
	$max   = intval($wp_query->max_num_pages);
	$links = array();
	if($paged >= 1) $links[] = $paged;
	if($paged >= 3){
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}
	if(($paged + 2) <= $max){
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}
	echo '<nav><ul class="pagination">';
	if(get_previous_posts_link()) printf('<li>%s</li>', get_previous_posts_link('<<'));
	if(!in_array(1, $links)){
		$class = (1 == $paged) ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>', $class, esc_url( get_pagenum_link(1)), '1' );
		if(!in_array( 2, $links)) echo '<li>…</li>';
	}
	sort( $links );
	foreach($links as $link){
		$class = ($paged == $link) ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>', $class, esc_url(get_pagenum_link($link)), $link);
	}
	if(!in_array( $max, $links)){
		if(!in_array($max - 1, $links )) echo '<li>…</li>';
		$class = ($paged == $max) ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>', $class, esc_url(get_pagenum_link($max)), $max );
	}
	if(get_next_posts_link()) printf( '<li>%s</li>', get_next_posts_link('>>'));
	echo '</ul></nav>';
}

function formatDateComment($date){
	$parts = explode(' ', $date);
	if(count($parts) == 2){
		$dates = explode('-', $parts[0]);
		if(count($dates) == 3) return $dates[2].'/'.$dates[1].'/'.$dates[0].' '.$parts[1];
	}
	return $date;
}

function getImageSrc($imageTag){
	preg_match('%<img.*?src=["\'](.*?)["\'].*?/>%i', $imageTag , $result);
	return $result[1];
}

// Theme Option Page
include_once(TEMPLATEPATH . '/functions/theme-options.php');
?>