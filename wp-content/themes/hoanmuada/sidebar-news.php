<?php $theme_directory = get_bloginfo('template_directory');?>
<div class="column col-xs-12 col-sm-3" id="left_column">
    <!-- Blog category -->
    <div class="block left-module">
        <p class="title_block">CHUYÊN MỤC</p>
        <div class="block_content">
            <div class="layered layered-category">
                <div class="layered-content">
                    <?php $menu = wp_nav_menu(array('echo' => false, 'theme_location' => 'product_nav', 'container'=>'','menu_class'=> 'tree-menu'));
                    echo str_replace('current-menu-item', 'current-menu-item active', $menu); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="block left-module">
        <p class="title_block">TIN TỨC MỚI NHẤT</p>
        <div class="block_content">
            <div class="layered">
                <div class="layered-content">
                    <ul class="blog-list-sidebar clearfiblock left-modulex">
                        <?php global $post;
                        $news = get_posts(array('posts_per_page'=> 3,'category'=> 7));
                        foreach($news as $post) : setup_postdata($post); ?>
                            <li>
                                <div class="post-thumb">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(array(70,49)); ?></a>
                                </div>
                                <div class="post-info">
                                    <h5 class="entry_title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                    <div class="post-meta">
                                        <span class="date"><i class="fa fa-calendar"></i> <?php the_time('d/m/y'); ?></span>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="block left-module">
        <div class="banner-opacity">
            <a href="#"><img src="<?php echo $theme_directory; ?>/assets/data/slide-left.jpg" alt="ads-banner"></a>
        </div>
    </div>
    <div class="block left-module">
        <p class="title_block">BÌNH LUẬN</p>
        <div class="block_content">
            <div class="layered">
                <div class="layered-content">
                    <ul class="recent-comment-list">
                        <?php $comments = get_comments(array('status' => 'approve', 'number' => 3));
                        foreach($comments as $c){ ?>
                        <li>
                            <?php $news = get_post($c->comment_post_ID); ?>
                            <h5><a href="<?php echo get_permalink($c->comment_post_ID); ?>"><?php echo $news->post_title; ?></a></h5>
                            <div class="comment">
                                "<?php echo $c->comment_content; ?>"
                            </div>
                            <div class="author">Bởi <a href="javarscript:void(0)"><?php echo $c->comment_author; ?></a></div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php $tags = get_tags();
    if($tags && !empty($tags)) : ?>
    <div class="block left-module">
        <p class="title_block">TAGS</p>
        <div class="block_content">
            <div class="tags">
                <?php foreach($tags as $tag){ ?>
                    <a href="<?php echo get_tag_link($tag->term_id); ?>"><span class="level5"><?php echo $tag->name; ?></span></a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="block left-module">
        <div class="banner-opacity">
            <a href="#"><img src="<?php echo $theme_directory; ?>/assets/data/slide-left2.jpg" alt="ads-banner"></a>
        </div>
    </div>
</div>