<?php
/*
Template Name: Contact Us
*/
?>
<?php get_header(); ?>
    <div class="columns-container">
        <div class="container" id="columns">
            <?php get_template_part('templates/breadcrumb'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <!-- page heading-->
            <h2 class="page-heading">
                <span class="page-heading-title2"><?php the_title(); ?></span>
            </h2>
            <!-- ../page heading-->
            <div id="contact" class="page-content page-contact">
                <!--<div id="message-box-conact"></div>-->
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="page-subheading">Liên hệ với chúng tôi</h3>
                        <div class="contact-form-box">
                            <?php echo do_shortcode('[contact-form-7 id="49" title="Contact"]'); ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6" id="contact_form_map">
                        <h3 class="page-subheading">Information</h3>
                        <p>Lorem ipsum dolor sit amet onsectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor dapibus eget. Mauris tincidunt aliquam lectus sed vestibulum. Vestibulum bibendum suscipit mattis.</p>
                        <br/>
                        <ul>
                            <li>Praesent nec tincidunt turpis.</li>
                            <li>Aliquam et nisi risus.&nbsp;Cras ut varius ante.</li>
                            <li>Ut congue gravida dolor, vitae viverra dolor.</li>
                        </ul>
                        <br/>
                        <ul class="store_info">
                            <li><i class="fa fa-home"></i><?php echo getMyOption('address'); ?></li>
                            <li><i class="fa fa-phone"></i><span><?php echo getMyOption('hotline'); ?></span></li>
                            <li><i class="fa fa-phone"></i><span><?php echo getMyOption('phone'); ?></span></li>
                            <li><i class="fa fa-envelope"></i>Email: <span><a href="mailto:<?php echo getMyOption('email'); ?>"><?php echo getMyOption('email'); ?></a></span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
    <input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>
