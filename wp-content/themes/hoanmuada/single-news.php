<?php get_header(); ?>
    <div class="columns-container">
        <div class="container" id="columns">
            <?php get_template_part('templates/breadcrumb'); ?>
            <div class="row">
                <?php get_sidebar('news'); ?>
                <?php $newsCateUrl = get_category_link(7); ?>
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="center_column col-xs-12 col-sm-9" id="center_column">
                    <h1 class="page-heading">
                        <span class="page-heading-title2"><?php the_title(); ?></span>
                    </h1>
                    <article class="entry-detail">
                        <div class="entry-meta-data">
                        <span class="author">
                        <i class="fa fa-user"></i>
                        <a href="#">Admin</a></span>
                        <span class="cat">
                            <i class="fa fa-folder-o"></i>
                            <a href="<?php echo $newsCateUrl; ?>">Tin tức</a>
                        </span>
                        <span class="comment-count">
                            <i class="fa fa-comment-o"></i> 3
                        </span>
                            <span class="date"><i class="fa fa-calendar"></i> <?php the_time('d/m/y H:i:s'); ?></span>
                        <!--<span class="post-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <span>(7 votes)</span>
                        </span>-->
                        </div>
                        <!--<div class="entry-photo">
                            <img src="assets/data/blog-full.jpg" alt="Blog">
                        </div>-->
                        <div class="content-text clearfix">
                            <?php the_content(); ?>
                        </div>
                    <?php $tags = get_the_tags();
                    if($tags && !empty($tags)) : ?>
                        <div class="entry-tags">
                            <span>Tags:</span>
                            <?php foreach($tags as $tag){ ?>
                                <a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name; ?>,</a>
                            <?php } ?>
                        </div>
                        <?php endif; ?>
                    </article>
                    <div class="single-box">
                        <h2>Tin liên quan</h2>
                        <ul class="related-posts owl-carousel" data-dots="false" data-loop="true" data-nav = "true" data-margin = "30" data-autoplayTimeout="1000" data-autoplayHoverPause = "true" data-responsive='{"0":{"items":1},"600":{"items":2},"1000":{"items":3}}'>
                            <?php global $post;
                            $news = get_posts(array('posts_per_page'=> 3,'category'=> 7, 'orderby' => 'rand'));
                            foreach($news as $post) : setup_postdata($post); ?>
                            <li class="post-item">
                                <article class="entry">
                                    <div class="entry-thumb image-hover2">
                                        <a href="<?php the_permalink(); ?>">
                                            <?php the_post_thumbnail(array(345, 244)); ?>
                                        </a>
                                    </div>
                                    <div class="entry-ci">
                                        <h3 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                        <div class="entry-meta-data">
                                        <span class="date">
                                            <i class="fa fa-calendar"></i> <?php the_time('d/m/y'); ?>
                                        </span>
                                        </div>
                                        <div class="entry-more">
                                            <a href="<?php the_permalink(); ?>">Đọc thêm</a>
                                        </div>
                                    </div>
                                </article>
                            </li>
                            <?php endforeach; wp_reset_postdata(); ?>
                        </ul>
                    </div>
                    <?php comments_template(); ?>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
    <input type="text" hidden="hidden" id="pageType" value="category">
<?php get_footer(); ?>