<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_tamvietson');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '8[BRX(FIuO+GC>7xt$Gnc8CJ2^ZV;H=|UDJKYJMTT3h?ua3k8k]C:foC3v%U`$uK');
define('SECURE_AUTH_KEY',  'AAPsIv(7qx_?Jgf/*dc|`]hX_A>wOUvn9uX{y#:-x<RO+,-/XYav>2r`z@]i5)nB');
define('LOGGED_IN_KEY',    'P1ho&e*ocCLQ{D{f%q(uVrF*n Nh&/[j6${4b.hS[RdL!J=)TY|{0xm0Ts+BWj%y');
define('NONCE_KEY',        '>ba D`(:Yqt31Fh&<B j!`=PGpEqCZMVM$LK[6} J|a+@E[l}{F2H.)1P2q+|A`4');
define('AUTH_SALT',        'Tcp6dw@k(qu6S;69vy-]^{8735<`3hNF-=.&):0vs,7oqt77{f50:Z@p$JO|4{*&');
define('SECURE_AUTH_SALT', ']C-JR8/m!#sV>&0e*/FVURBvTHdINfqRis$a9lE1)xR>v -hd[fEuz(VD[1GrNvM');
define('LOGGED_IN_SALT',   'DT7>/I@~y!2@C]&HoC%2U$6-Rmeax(or61g(X?Qub2YCyF+OGqUy82Kv9g1#dxyS');
define('NONCE_SALT',       '+WoEI$37T]TLu7POp>m|p>kYWLwmSUuCC?rC(,iZ;eV6oiW9xnkW=zD?YuRmZBB_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
